﻿/// <reference path="../jquery-1.10.2.js" />
/// <reference path="../underscore.js" />
/// <reference path="../backbone.js" />

var AddModel = Backbone.Model.extend({
    urlRoot: '/My/Index',
    url: '/My/Index'
});

var addModel = new AddModel();

var AddView = Backbone.View.extend({
    el: $("#Content"),
    model: addModel,
    events: {

    },
    initialize: function () {
        this.render();
    },

    render: function () {

    },
    add: function () {

    }
})

var addView = new AddView();