﻿/// <reference path="../jquery-1.10.2.js" />
/// <reference path="../underscore.js" />
/// <reference path="../backbone.js" />

var AddModel = Backbone.Model.extend({
    urlRoot: '/My/Index',
    url: '/My/Index'
});

var addModel = new AddModel();

var AddView = Backbone.View.extend({
    el: $("#Content"),
    model: addModel,
    events: {
        "click #add_button": "add"
    },
    initialize: function () {
        this.render();
    },

    render: function () {
        var template = _.template($("#addTemplate").html());
        this.$el.html(template);
    },
    add: function () {
        this.model.set({
            FirstName: this.$el.find('#addFirstName').val(),
            LastName: this.$el.find('#addLastName').val()
        })
        this.model.save();
    }
})

var addView = new AddView();