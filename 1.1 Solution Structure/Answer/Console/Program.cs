﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new CustomerRepository();
            Customer customer = repository.GetCustomer();
            System.Console.WriteLine(customer.Name);

            System.Console.WriteLine("Press any key to stop...");
            System.Console.ReadKey();
        }
    }
}
