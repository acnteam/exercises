﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventsTutorial
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOpenTrafficLightsController_Click(object sender, EventArgs e)
        {
            var trafficLightsController = new TrafficLightsController();
            trafficLightsController.Show();
        }

        private void btnAddTraficLightsDisplay_Click(object sender, EventArgs e)
        {
            var trafficLightsDisplay = new TrafficLightsDisplay();
            trafficLightsDisplay.Show();
        }
    }
}
