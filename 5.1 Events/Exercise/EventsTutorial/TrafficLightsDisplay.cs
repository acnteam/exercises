﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventsTutorial
{
    public partial class TrafficLightsDisplay : Form
    {
        public TrafficLightsDisplay()
        {
            InitializeComponent();
            SetTraficLightColor(TrafficLightColor.Red);
        }

        private void SetTraficLightColor(TrafficLightColor color)
        {
            switch (color)
            {
                case TrafficLightColor.Red:
                    pnlRed.BackColor = Color.Red;
                    pnlYellow.BackColor = SystemColors.Control;
                    pnlGreen.BackColor = SystemColors.Control;
                    break;
                case TrafficLightColor.Yellow:
                    pnlRed.BackColor = SystemColors.Control;
                    pnlYellow.BackColor = Color.Yellow;
                    pnlGreen.BackColor = SystemColors.Control;
                    break;
                case TrafficLightColor.Green:
                    pnlRed.BackColor = SystemColors.Control;
                    pnlYellow.BackColor = SystemColors.Control;
                    pnlGreen.BackColor = Color.Green;
                    break;
            }
        }
    }
}
