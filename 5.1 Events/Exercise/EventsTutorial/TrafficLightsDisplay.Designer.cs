﻿namespace EventsTutorial
{
    partial class TrafficLightsDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlRed = new System.Windows.Forms.Panel();
            this.pnlYellow = new System.Windows.Forms.Panel();
            this.pnlGreen = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlRed
            // 
            this.pnlRed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRed.Location = new System.Drawing.Point(50, 28);
            this.pnlRed.Name = "pnlRed";
            this.pnlRed.Size = new System.Drawing.Size(63, 47);
            this.pnlRed.TabIndex = 0;
            // 
            // pnlYellow
            // 
            this.pnlYellow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlYellow.Location = new System.Drawing.Point(50, 81);
            this.pnlYellow.Name = "pnlYellow";
            this.pnlYellow.Size = new System.Drawing.Size(63, 47);
            this.pnlYellow.TabIndex = 1;
            // 
            // pnlGreen
            // 
            this.pnlGreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGreen.Location = new System.Drawing.Point(50, 134);
            this.pnlGreen.Name = "pnlGreen";
            this.pnlGreen.Size = new System.Drawing.Size(63, 47);
            this.pnlGreen.TabIndex = 2;
            // 
            // TrafficLightsDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(168, 220);
            this.Controls.Add(this.pnlGreen);
            this.Controls.Add(this.pnlYellow);
            this.Controls.Add(this.pnlRed);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "TrafficLightsDisplay";
            this.Text = "Światła";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRed;
        private System.Windows.Forms.Panel pnlYellow;
        private System.Windows.Forms.Panel pnlGreen;
    }
}

