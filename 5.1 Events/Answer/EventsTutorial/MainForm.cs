﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventsTutorial
{
    public partial class MainForm : Form
    {
        private readonly TrafficLightModel _model;

        public MainForm(TrafficLightModel model)
        {
            _model = model;
            InitializeComponent();
        }

        private void btnOpenTrafficLightsController_Click(object sender, EventArgs e)
        {
            var trafficLightsController = new TrafficLightsController(_model);
            trafficLightsController.Show();
        }

        private void btnAddTraficLightsDisplay_Click(object sender, EventArgs e)
        {
            var trafficLightsDisplay = new TrafficLightsDisplay(_model);
            trafficLightsDisplay.Show();
        }
    }
}
