﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsTutorial
{
    public class TrafficLightModel
    {
        private TrafficLightColor _lightColor = TrafficLightColor.Red;

        public delegate void ChangedLightEventHandler(object sender, TrafficLightEventArgs e);

        public event ChangedLightEventHandler TrafficLightsChanged;

        public TrafficLightColor LightColor
        {
            get { return _lightColor; }
            private set { _lightColor = value; }
        }

        protected virtual void OnTrafficLightsChanged(TrafficLightEventArgs e)
        {
            ChangedLightEventHandler handler = TrafficLightsChanged;
            if (handler != null) handler(this, e);
        }

        public TrafficLightModel(TrafficLightColor lightColor)
        {
            LightColor = lightColor;
        }

        public void ChangeTrafficLights(TrafficLightColor newColor)
        {
            LightColor = newColor;
            var args = new TrafficLightEventArgs {NewColor = newColor};
            OnTrafficLightsChanged(args);
        }
    }

    public class TrafficLightEventArgs : EventArgs
    {
        public TrafficLightColor NewColor { get; set; }
    }
}
