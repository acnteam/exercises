﻿namespace EventsTutorial
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenTrafficLightsController = new System.Windows.Forms.Button();
            this.btnAddTraficLightsDisplay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenTrafficLightsController
            // 
            this.btnOpenTrafficLightsController.Location = new System.Drawing.Point(48, 47);
            this.btnOpenTrafficLightsController.Name = "btnOpenTrafficLightsController";
            this.btnOpenTrafficLightsController.Size = new System.Drawing.Size(122, 23);
            this.btnOpenTrafficLightsController.TabIndex = 0;
            this.btnOpenTrafficLightsController.Text = "Otwórz kontroler";
            this.btnOpenTrafficLightsController.UseVisualStyleBackColor = true;
            this.btnOpenTrafficLightsController.Click += new System.EventHandler(this.btnOpenTrafficLightsController_Click);
            // 
            // btnAddTraficLightsDisplay
            // 
            this.btnAddTraficLightsDisplay.Location = new System.Drawing.Point(199, 47);
            this.btnAddTraficLightsDisplay.Name = "btnAddTraficLightsDisplay";
            this.btnAddTraficLightsDisplay.Size = new System.Drawing.Size(122, 23);
            this.btnAddTraficLightsDisplay.TabIndex = 1;
            this.btnAddTraficLightsDisplay.Text = "Dodaj światła";
            this.btnAddTraficLightsDisplay.UseVisualStyleBackColor = true;
            this.btnAddTraficLightsDisplay.Click += new System.EventHandler(this.btnAddTraficLightsDisplay_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 121);
            this.Controls.Add(this.btnAddTraficLightsDisplay);
            this.Controls.Add(this.btnOpenTrafficLightsController);
            this.Name = "MainForm";
            this.Text = "System sterowania świateł";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenTrafficLightsController;
        private System.Windows.Forms.Button btnAddTraficLightsDisplay;
    }
}