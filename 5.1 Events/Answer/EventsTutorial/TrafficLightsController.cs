﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventsTutorial
{
    public partial class TrafficLightsController : Form
    {
        private readonly TrafficLightModel _model;

        public TrafficLightsController(TrafficLightModel model)
        {
            _model = model;
            InitializeComponent();
        }

        private void btnRed_Click(object sender, EventArgs e)
        {
            _model.ChangeTrafficLights(TrafficLightColor.Red);
        }

        private void btnYellow_Click(object sender, EventArgs e)
        {
            _model.ChangeTrafficLights(TrafficLightColor.Yellow);
        }

        private void btnGreen_Click(object sender, EventArgs e)
        {
            _model.ChangeTrafficLights(TrafficLightColor.Green);
        }
    }
}
