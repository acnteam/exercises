﻿using MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class MyController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            Person person = new Person();
            person.FirstName = "Pawel";
            person.LastName = "Wujczyk";

            return View(person);
        }

        [HttpPost]
        public ActionResult Index(Person person)
        {
            return View(person);
        }
	}
}