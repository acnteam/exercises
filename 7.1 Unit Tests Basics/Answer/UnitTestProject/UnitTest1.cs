﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestBasics;

namespace UnitTestBasicsTest
{
    [TestClass]
    public class PeselLogicTest
    {
        [TestMethod]
        public void Pesel_GetChecksum_ReturnsCorrectValue()
        {
            var peselLogic = new PeselLogic();
            string result = peselLogic.CalculateCheckSum("8410091529");

            Assert.AreEqual(result, "5");
        }

        [TestMethod]
        public void Pesel_InvalidChecksum_ReturnsFalse()
        {
            var peselLogic = new PeselLogic();
            bool result = peselLogic.IsPeselValid("84100915296");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Pesel_Valid_ReturnsFalse()
        {
            var peselLogic = new PeselLogic();
            bool result = peselLogic.IsPeselValid("84100915295");

            Assert.IsTrue(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Pesel_NullValue_ThrowsInvalidArgumentException()
        {
            var peselLogic = new PeselLogic();
            peselLogic.IsPeselValid(null);
        }

        [TestMethod]
        public void Pesel_Sex_IsCorrectForMale()
        {
            var peselLogic = new PeselLogic();
            Sex result = peselLogic.GetSex("84100915295");

            Assert.AreEqual(result, Sex.Male);
        }
    }
}
