﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace ServiceTools
{
    [AttributeUsage(AttributeTargets.Method)]
    public class PWBehaviour : Attribute, IOperationBehavior, IEndpointBehavior
    {
        private string callerSystemName = "callerSystemName";
        //protected PWInvoker CreateInvoker(IOperationInvoker oldInvoker);

        public void ApplyDispatchBehavior(OperationDescription operationDescription,
                                          DispatchOperation dispatchOperation)
        {

            var callerSystemNameParam=operationDescription.Messages[0].Body.Parts.FirstOrDefault(x => x.Name == callerSystemName);
            if (callerSystemNameParam==null)
            {
                throw new Exception(string.Concat("Missing ", callerSystemNameParam));
            }
            

            //string[] inputParamNames = operationDescription.Messages[0].Body.Parts
            //        .OrderBy(mpd => mpd.Index)
            //        .Select(mpd => mpd.Name)
            //        .ToArray();

            //IOperationInvoker oldInvoker = dispatchOperation.Invoker;
            //dispatchOperation.Invoker = new PWInvoker(dispatchOperation.Invoker);// CreateInvoker(oldInvoker);
            dispatchOperation.ParameterInspectors.Add(new PWParameterInceptor(callerSystemNameParam.Index));
            
        }
        //More methods

        public void AddBindingParameters(OperationDescription operationDescription, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            //throw new NotImplementedException();
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
            ///throw new NotImplementedException();
        }

        public void Validate(OperationDescription operationDescription)
        {
            //throw new NotImplementedException();
        }

        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            throw new NotImplementedException();
        }

        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            throw new NotImplementedException();
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new ConsoleMessageTracer());
        }

        void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
        {
            throw new NotImplementedException();
        }
    }
}
