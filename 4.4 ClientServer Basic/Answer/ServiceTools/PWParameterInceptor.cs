﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace ServiceTools
{
    class PWParameterInceptor : IParameterInspector
    {
        int CallerSystemNameIndex;

        public PWParameterInceptor(int callerSystemNameIndex)
        {
            this.CallerSystemNameIndex = callerSystemNameIndex;
        }
        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            //throw new NotImplementedException();
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            string callerSystemName = inputs[CallerSystemNameIndex].ToString();
            // throw new NotImplementedException();
            return null;
        }
    }

    public class ConsoleMessageTracer : IDispatchMessageInspector,
IClientMessageInspector
    {
        private Message TraceMessage(MessageBuffer buffer)
        {
            Message msg = buffer.CreateMessage();
            Console.WriteLine("\n{0}\n", msg);
            return buffer.CreateMessage();
        }
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            request = TraceMessage(request.CreateBufferedCopy(int.MaxValue));
            return null;
        }
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            reply = TraceMessage(reply.CreateBufferedCopy(int.MaxValue));
        }
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            reply = TraceMessage(reply.CreateBufferedCopy(int.MaxValue));
        }
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            request = TraceMessage(request.CreateBufferedCopy(int.MaxValue));
            return null;
        }
    }
}
