﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace ServiceTools
{
    public class PWInvoker :IOperationInvoker
    {
        IOperationInvoker OperationInvoker;

        public PWInvoker(IOperationInvoker operationInvoker)
        {
            this.OperationInvoker = operationInvoker;
        }

        public object[] AllocateInputs()
        {
            //throw new NotImplementedException();
            return OperationInvoker.AllocateInputs();
            return null;
            return new object[1];
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
           // OperationInvoker as System.ServiceModel.Dispatcher.Sync SyncMethodInvoker
            return OperationInvoker.Invoke(instance, inputs,out outputs);
            ////throw new NotImplementedException();
            //outputs=new object[1];
            //inputs=new object[1];
            //return new object();
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
           throw new NotImplementedException();
        }

        public bool IsSynchronous
        {
            get { return true; }
        }
    }
}
