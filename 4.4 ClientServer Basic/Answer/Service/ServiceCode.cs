﻿using Contract;
using ServiceTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ServiceCode : IService
    {
        public int Add(int a, int b)
        {
            return a + b;
        }
    }
}
