﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            ChannelFactory<IService> factory = new ChannelFactory<IService>(new NetTcpBinding(), "net.tcp://localhost:6001/");
            IService proxy = factory.CreateChannel();
            int result =proxy.Add(1, 2);
            Console.WriteLine(result);
            Console.Read();
        }
    }
}
