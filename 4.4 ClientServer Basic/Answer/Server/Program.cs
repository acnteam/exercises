﻿using Contract;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            NetTcpBinding b = new NetTcpBinding();
            ServiceHost host = new ServiceHost(typeof(ServiceCode));
            host.AddServiceEndpoint(typeof(IService), b, "net.tcp://localhost:6001/");
            host.Open();
            Console.WriteLine("ServiceOpened");

            Console.Read();
        }
    }
}
