﻿using System;
using System.ComponentModel;
using System.Messaging;
using System.Threading;
using System.Windows.Forms;
using Message = System.Messaging.Message;

namespace RealTimeMarketing
{
    public partial class AddGeneratorForm : Form
    {

        private static bool keepGoing = true;
        private Thread thread;

        public AddGeneratorForm()
        {
            InitializeComponent();
            thread = AddWatchingThread(".\\private$\\rtmqueue");

        }


        public void AsyncWatchQueue(object inQueueName)
        {

            MessageQueue queue = null;

            string queueName = inQueueName as string;
            if (queueName == null)
                return;

            try
            {
                //Inicjalizacja z kolejki

                while (keepGoing)
                {
                    //TODO: Odczyt z kolejki
                }
            }

            catch (Exception e)
            {
                //Np ubicie wątku
                MessageBox.Show(e.Message, "My Application",MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (queue != null)
                    queue.Dispose();
            }
        }

        private void HandleMessage(Message incommingTransfer)
        {
            //TODO: Wyświetlenie info o przelewach o kwocie większej niż 90PLN
        }

        public Thread AddWatchingThread(string inQueueName)
        {
            Thread Watcher =
                new Thread(new ParameterizedThreadStart(AsyncWatchQueue));
            Watcher.Start(inQueueName);

            return Watcher;
        }

        private void AddGeneratorForm_FormClosing(object sender, EventArgs e)
        {
            keepGoing = false;
            thread.Abort();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            thread.Abort();
        }
    }
}
