﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealTimeMarketing
{
    [Serializable]
    public class Transfer
    {
        public string Title;
        public decimal Amount;
    }
}
