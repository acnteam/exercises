﻿using System;
using System.ComponentModel;
using System.Messaging;
using System.Threading;
using System.Windows.Forms;
using Message = System.Messaging.Message;

namespace RealTimeMarketing
{
    public partial class AddGeneratorForm : Form
    {

        private static bool keepGoing = true;
        private Thread thread;

        public AddGeneratorForm()
        {
            InitializeComponent();
            thread = AddWatchingThread(".\\private$\\rtmqueue");

        }


        public void AsyncWatchQueue(object encapsulatedQueueName)
        {
            Message newMessage = new Message();
            MessageQueue queue = null;

            string queueName = encapsulatedQueueName as string;
            if (queueName == null)
                return;

            try
            {
                if (!MessageQueue.Exists(queueName))
                    MessageQueue.Create(queueName);
 
                queue = new MessageQueue(queueName);

                queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(Transfer) });

                while (keepGoing)
                {
                    if (queue.CanRead)
                        newMessage = queue.Receive();

                    Transfer incommingTransfer = (Transfer) newMessage.Body;
                    HandleMessage(incommingTransfer);
                }
            }

            catch (Exception e)
            {
                //Np ubicie wątku
                MessageBox.Show(e.Message, "My Application",MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (queue != null)
                    queue.Dispose();
            }
        }

        private void HandleMessage(Transfer incommingTransfer)
        {
            if (incommingTransfer.Amount > 90)
            {
                string tekst = string.Format("Otrzymano przelew na {0} PLN", incommingTransfer.Amount);
                MessageBox.Show(tekst, "Duży przelew",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation)
                ;
            }
        }

        public Thread AddWatchingThread(string QueueName)
        {
            Thread Watcher =
                new Thread(new ParameterizedThreadStart(AsyncWatchQueue));
            Watcher.Start(QueueName);

            return Watcher;
        }

        private void AddGeneratorForm_FormClosing(object sender, EventArgs e)
        {
            keepGoing = false;
            thread.Abort();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            thread.Abort();
        }
    }
}
