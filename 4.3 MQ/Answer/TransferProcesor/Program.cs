﻿using System;
using System.Messaging;
using RealTimeMarketing;

namespace TransferProcesor
{
    class Program
    {
        static void Main(string[] args)
        {
            const string queueName = ".\\private$\\rtmqueue";
            Random rnd =new Random();
            for (int i = 0;i<100;i++)
            {

                MessageQueue messageQueue = null;
                if (MessageQueue.Exists(queueName))
                {
                    messageQueue = new MessageQueue(queueName);
  
                }
                else
                {
                    MessageQueue.Create(queueName);
                    messageQueue = new MessageQueue(queueName);
                }
                Transfer transfer = new Transfer(){Amount = rnd.Next(0,100),Title = "Wplata wlasna"};
                System.Console.WriteLine("Amount={0} Title={1}", transfer.Amount, transfer.Title);
                messageQueue.Send(transfer);
            }

            System.Console.WriteLine("Press <Enter> to stop the service.");
            System.Console.ReadLine();
        }
    }
}

