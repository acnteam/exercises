﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var converter = CurrencyConverter.Instance;
            Console.WriteLine(converter.ConvertToPln("USD", 1));
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
