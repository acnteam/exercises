﻿using System;
using System.Collections.Generic;

namespace SingletonConsole
{
    public class CurrencyConverter
    {
        private static volatile CurrencyConverter _instance;
        private static readonly object SyncRoot = new object();

        private readonly Dictionary<string, decimal> _currencyRates = new Dictionary<string, decimal>();

        private CurrencyConverter()
        {
            RefreshCurrencyList();
        }

        public static CurrencyConverter Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new CurrencyConverter();
                    }
                }

                return _instance;
            }
        }

        public decimal ConvertToPln(string currencyCode, decimal amount)
        {
            if (!_currencyRates.ContainsKey(currencyCode))
                throw new Exception("Currency: " + currencyCode + " is not supported.");

            return _currencyRates[currencyCode] * amount;
        }

        private void RefreshCurrencyList()
        {
            var reader = new MockCurrencyRateReader();
            List<CurrencyRate> currencyList = reader.GetCurrencyRates();

            _currencyRates.Clear();
            foreach (var rate in currencyList)
            {
                _currencyRates.Add(rate.CurrencyCode, rate.Rate);
            }
        }
    }
}
