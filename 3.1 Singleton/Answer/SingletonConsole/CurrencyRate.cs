﻿namespace SingletonConsole
{
    public struct CurrencyRate
    {
        public string CurrencyCode;
        public decimal Rate;
    }
}
