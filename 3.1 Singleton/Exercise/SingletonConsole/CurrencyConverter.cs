﻿using System;
using System.Collections.Generic;

namespace SingletonConsole
{
    public class CurrencyConverter
    {
        private readonly Dictionary<string, decimal> _currencyRates = new Dictionary<string, decimal>();

        public CurrencyConverter()
        {
            RefreshCurrencyList();
        }

        public decimal ConvertToPln(string currencyCode, decimal amount)
        {
            if (!_currencyRates.ContainsKey(currencyCode))
                throw new Exception("Currency: " + currencyCode + " is not supported.");

            return _currencyRates[currencyCode] * amount;
        }

        private void RefreshCurrencyList()
        {
            var reader = new MockCurrencyRateReader();
            List<CurrencyRate> currencyList = reader.GetCurrencyRates();

            _currencyRates.Clear();
            foreach (var rate in currencyList)
            {
                _currencyRates.Add(rate.CurrencyCode, rate.Rate);
            }
        }
    }
}
