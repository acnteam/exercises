﻿using System.Collections.Generic;

namespace SingletonConsole
{
    public class MockCurrencyRateReader
    {
        public List<CurrencyRate> GetCurrencyRates()
        {
            var currencyList = new List<CurrencyRate>()
            {
                new CurrencyRate() {CurrencyCode = "PLN", Rate = 1m},
                new CurrencyRate() {CurrencyCode = "EUR", Rate = 4.3m},
                new CurrencyRate() {CurrencyCode = "USD", Rate = 3.3m}
            };
            return currencyList;
        }
    }
}