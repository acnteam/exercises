﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileWriting
{
    class Program
    {
        private const string Separator = "|";

        static void Main(string[] args)
        {
            var rateList = new List<CurrencyRate>();
            rateList.Add(new CurrencyRate() { CurrencyCode = "PLN", Rate = 1m });
            rateList.Add(new CurrencyRate() { CurrencyCode = "USD", Rate = 3.3m });
            rateList.Add(new CurrencyRate() { CurrencyCode = "EUR", Rate = 4.3m });

            // Save file

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
