﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileParsing
{
    class Program
    {
        private const char Separator = '|';

        static void Main(string[] args)
        {
            var rates = new List<CurrencyRate>();
            using (TextReader reader = new StreamReader("PipedRates.txt"))
            {
                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    String[] elements = line.Split(Separator);
                    var rate = new CurrencyRate
                    {
                        CurrencyCode = elements[0],
                        Rate = decimal.Parse(elements[1])
                    };
                    rates.Add(rate);
                }
            }
            
            foreach (CurrencyRate rate in rates)
                Console.WriteLine(rate.CurrencyCode + " " + rate.Rate);

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
