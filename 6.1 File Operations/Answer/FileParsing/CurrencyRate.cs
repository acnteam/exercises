﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileParsing
{
    public struct CurrencyRate
    {
        public string CurrencyCode;
        public decimal Rate;
    }
}
