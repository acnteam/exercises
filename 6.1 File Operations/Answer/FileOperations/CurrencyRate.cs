﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileOperations
{
    public struct CurrencyRate
    {
        public string CurrencyCode;
        public decimal Rate;
    }
}
