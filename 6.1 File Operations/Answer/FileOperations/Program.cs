﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using FileOperations.Readers;

namespace FileOperations
{
    class Program
    {
        static void Main(string[] args)
        {
            ICurrencyRateReader fileReader = new CurrencyRateFileReader();
            List<CurrencyRate> ratesFromFile = fileReader.GetCurrencyRates();

            ICurrencyRateReader serviceReader = new CurrencyRateFileReader();
            List<CurrencyRate> ratesFromService = serviceReader.GetCurrencyRates();

            var writer = new StringWriter();
            var serializer = new XmlSerializer(typeof(List<CurrencyRate>));
            serializer.Serialize(writer, ratesFromFile);

            File.WriteAllText("outputRates.txt", writer.GetStringBuilder().ToString());
        }
    }
}
