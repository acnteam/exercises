﻿using System.Xml.Linq;
using Abatap.AppSrv.Business.Engine.Currency.Readers;

namespace FileOperations.Readers
{
    public class CurrencyRateFileReader : AbstractEbcCurrencyRateReader
    {
        protected override XDocument GetRatesFile()
        {
            return XDocument.Load("CurrencyRates.xml");
        }
    }
}
