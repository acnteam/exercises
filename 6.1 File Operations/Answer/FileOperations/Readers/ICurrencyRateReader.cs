﻿using System.Collections.Generic;

namespace FileOperations.Readers
{
    public interface ICurrencyRateReader
    {
        List<CurrencyRate> GetCurrencyRates();
    }
}
