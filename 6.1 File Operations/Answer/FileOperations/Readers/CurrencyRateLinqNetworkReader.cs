﻿using System.Net;
using System.Xml.Linq;
using Abatap.AppSrv.Business.Engine.Currency.Readers;

namespace FileOperations.Readers
{
    public class CurrencyRateLinqReader : AbstractEbcCurrencyRateReader
    {
        private const string EbcRatesUrl = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

        protected override XDocument GetRatesFile()
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(EbcRatesUrl);
            XDocument xdoc = XDocument.Parse(content);
            return xdoc;
        }
    }
}
