﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    public class PeselDuplicatedException : Exception
    {
        public PeselDuplicatedException(string peselNumber)
            : base("Pesel '" + peselNumber + "' istnieje już w repozytorium.")
        {
            
        }
    }
}
