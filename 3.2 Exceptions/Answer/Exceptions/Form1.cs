﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exceptions
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAddPesel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string pesel = textBox1.Text;
                PeselRepository.AddPesel(pesel);
                listBox1.DataSource = null;
                listBox1.DataSource = PeselRepository.PeselList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nie można dodać peselu do repozytorium. Przyczyna: " + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
