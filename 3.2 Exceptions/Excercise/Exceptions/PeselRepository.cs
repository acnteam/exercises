﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exceptions
{
    public class PeselRepository
    {
        public static List<string> PeselList = new List<string>();

        public static void AddPesel(string pesel)
        {
            var validator = new PeselValidator();
            validator.ValidatePesel(pesel);

            PeselList.Add(pesel);
        }
    }
}
