﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToSql
{
    class Program
    {
        static void Main(string[] args)
        {
            var ctx = new DataClassesDataContext();

            int customerCount = ctx.Customers.Count();
            Console.WriteLine("Liczba klientów to: " + customerCount);

            // Insert Customer
            var customer = new Customer()
            {
                Name = "Marek",
                Surname = "Abacki",
                Phone = "123123123",
                PersonalId = "78121109123",
                LastModDate = DateTime.Now,
                LastModUser = "jan.kowalski"
            };
            ctx.Customers.InsertOnSubmit(customer);

            customerCount = ctx.Customers.Count();
            Console.WriteLine("Liczba klientów to: " + customerCount);

            ctx.SubmitChanges();

            customerCount = ctx.Customers.Count();
            Console.WriteLine("Liczba klientów to: " + customerCount);

            List<Customer> customersWithAccount = ctx.Accounts.Where(a => a.Balance > 0).Select(a => a.Customer).ToList();
            foreach (var customerWithAccount in customersWithAccount)
                Console.WriteLine("Klient: " + customerWithAccount.Name + " " + customerWithAccount.Surname + " posiada rachunek.");

            ctx.Customers.DeleteOnSubmit(customer);
            ctx.SubmitChanges();

            customerCount = ctx.Customers.Count();
            Console.WriteLine("Liczba klientów to: " + customerCount);

            Console.WriteLine("Press any key to stop...");
            Console.ReadKey();
        }
    }
}
