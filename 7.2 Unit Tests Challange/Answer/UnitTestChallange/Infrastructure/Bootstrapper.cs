﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using UnitTestChallange.BusinessLogic;
using UnitTestChallange.DatabaseLayer;

namespace UnitTestChallange.Infrastructure
{
    public class Bootstrapper
    {
        public IUnityContainer CreateContainer()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterType<IPeselRepository, PeselRepository>();
            container.RegisterType<PeselManager>();

            return container;
        }
    }
}
