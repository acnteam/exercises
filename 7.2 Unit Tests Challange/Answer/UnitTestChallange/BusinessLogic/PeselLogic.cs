﻿using System;

namespace UnitTestChallange
{
    public class PeselLogic
    {
        private static readonly int[] Multipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

        public bool IsPeselValid(string pesel)
        {
            bool isPeselValid = false;
            try
            {
                if (pesel.Length == 11)
                {
                    string checksum = CalculateCheckSum(pesel);
                    isPeselValid = checksum.Equals(pesel[10].ToString());
                }
            }
            catch (Exception)
            {
                isPeselValid = false;
            }
            return isPeselValid;
        }

        public string CalculateCheckSum(string pesel)
        {
            int sum = 0;
            for (int i = 0; i < Multipliers.Length; i++)
            {
                sum += Multipliers[i] * int.Parse(pesel[i].ToString());
            }
            int ramaining = sum % 10;
            string checkSum = ramaining == 0 ? ramaining.ToString() : (10 - ramaining).ToString();
            return checkSum;
        }

        public Sex GetSex(string pesel)
        {
            char sexCharacter = pesel[9];
            int parsedSexCharacter = int.Parse(sexCharacter.ToString());
            if (parsedSexCharacter % 2 == 0)
            {
                return Sex.Female;
            }
            return Sex.Male;
        }
    }
}
