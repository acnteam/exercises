﻿using System;
using UnitTestChallange.DatabaseLayer;

namespace UnitTestChallange.BusinessLogic
{
    public class PeselManager
    {
        private readonly IPeselRepository _repository;

        public PeselManager(IPeselRepository repository)
        {
            _repository = repository;
        }

        public PeselRegistrationResult RegisterPesel(string pesel)
        {
            var result = new PeselRegistrationResult();

            var peselProcessor = new PeselLogic();

            result.IsValid = peselProcessor.IsPeselValid(pesel);
            if (result.IsValid)
            {
                Sex sex = peselProcessor.GetSex(pesel);
                result.SexDescription = sex == Sex.Female ? "Kobieta" : "Mężczyzna";
            }

            _repository.SavePesel(pesel);

            return result;
        }
    }
}
