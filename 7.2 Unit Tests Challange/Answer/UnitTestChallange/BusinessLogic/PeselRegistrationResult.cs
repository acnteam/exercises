﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestChallange.BusinessLogic
{
    public class PeselRegistrationResult
    {
        public bool IsValid { get; set; }
        public string SexDescription { get; set; }
    }
}
