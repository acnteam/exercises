﻿using System;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using UnitTestChallange.BusinessLogic;
using UnitTestChallange.DatabaseLayer;
using UnitTestChallange.Infrastructure;

namespace UnitTestChallange
{
    public partial class CustomerForm : Form
    {
        private readonly IUnityContainer _container;

        public CustomerForm(IUnityContainer container)
        {
            _container = container;
            InitializeComponent();
        }

        private void btnPesel_Click(object sender, EventArgs e)
        {
            var peselManager = _container.Resolve<PeselManager>();

            var result = peselManager.RegisterPesel(txtPesel.Text);
            if (result.IsValid)
                lblSex.Text = result.SexDescription;
            else
                lblSex.Text = "Niepoprawny Pesel";
        }
    }
}
