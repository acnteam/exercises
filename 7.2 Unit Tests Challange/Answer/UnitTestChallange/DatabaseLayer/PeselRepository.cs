﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestChallange.DatabaseLayer
{
    public class PeselRepository : IPeselRepository
    {
        public void SavePesel(string pesel)
        {
            var ctx = new PeselDbDataContext();
            var registry = new PeselRegistry()
            {
                Pesel = pesel
            };
            ctx.PeselRegistries.InsertOnSubmit(registry);
            ctx.SubmitChanges();
        }
    }
}
