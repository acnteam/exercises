﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestChallange.DatabaseLayer;

namespace UnitTestProject.Mocks
{
    public class PeselRepositoryMock : IPeselRepository
    {
        public int SavedPesels { get; set; }

        public void SavePesel(string pesel)
        {
            SavedPesels++;
        }
    }
}
