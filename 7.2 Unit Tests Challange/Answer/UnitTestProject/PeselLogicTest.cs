﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestChallange;
using UnitTestChallange.DatabaseLayer;
using UnitTestProject.Mocks;

namespace UnitTestProject
{
    [TestClass]
    public class PeselLogicTest
    {

        [TestMethod]
        public void CalculateCheckSumTest()
        {
            var peselLogic = new PeselLogic();
            string checksum = peselLogic.CalculateCheckSum("84100915295");
            Assert.AreEqual(checksum, "5");
        }

        [TestMethod]
        public void GetSexTest()
        {

            var peselLogic = new PeselLogic();
            Sex sex = peselLogic.GetSex("84100915295");
            Assert.AreEqual(sex, Sex.Male);
        }
    }
}
