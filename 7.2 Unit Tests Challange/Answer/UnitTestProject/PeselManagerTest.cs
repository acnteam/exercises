﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestChallange.BusinessLogic;
using UnitTestChallange.DatabaseLayer;
using UnitTestProject.Mocks;

namespace UnitTestProject
{
    [TestClass]
    public class PeselManagerTest
    {
        [TestMethod]
        public void RegisterPeselTest()
        {
            var mockedRepository = new PeselRepositoryMock();
            var peselManager = new PeselManager(mockedRepository);
            peselManager.RegisterPesel("84100915295");
            Assert.AreEqual(mockedRepository.SavedPesels, 1);
        }
    }
}
