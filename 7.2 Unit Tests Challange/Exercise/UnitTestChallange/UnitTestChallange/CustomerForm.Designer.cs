﻿namespace UnitTestChallange
{
    partial class CustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPesel = new System.Windows.Forms.TextBox();
            this.btnPesel = new System.Windows.Forms.Button();
            this.lblPesel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPesel
            // 
            this.txtPesel.Location = new System.Drawing.Point(140, 26);
            this.txtPesel.Name = "txtPesel";
            this.txtPesel.Size = new System.Drawing.Size(163, 20);
            this.txtPesel.TabIndex = 0;
            // 
            // btnPesel
            // 
            this.btnPesel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPesel.Location = new System.Drawing.Point(323, 72);
            this.btnPesel.Name = "btnPesel";
            this.btnPesel.Size = new System.Drawing.Size(75, 23);
            this.btnPesel.TabIndex = 1;
            this.btnPesel.Text = "Zapisz";
            this.btnPesel.UseVisualStyleBackColor = true;
            this.btnPesel.Click += new System.EventHandler(this.btnPesel_Click);
            // 
            // lblPesel
            // 
            this.lblPesel.AutoSize = true;
            this.lblPesel.Location = new System.Drawing.Point(35, 32);
            this.lblPesel.Name = "lblPesel";
            this.lblPesel.Size = new System.Drawing.Size(71, 13);
            this.lblPesel.TabIndex = 2;
            this.lblPesel.Text = "Podaj PESEL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Twoja płeć to:";
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(140, 76);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(0, 13);
            this.lblSex.TabIndex = 4;
            // 
            // CustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 107);
            this.Controls.Add(this.lblSex);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPesel);
            this.Controls.Add(this.btnPesel);
            this.Controls.Add(this.txtPesel);
            this.Name = "CustomerForm";
            this.Text = "Dane klienta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPesel;
        private System.Windows.Forms.Button btnPesel;
        private System.Windows.Forms.Label lblPesel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSex;
    }
}

