﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace UnitTestChallange
{
    public partial class CustomerForm : Form
    {
        private static readonly int[] Multipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

        public CustomerForm()
        {
            InitializeComponent();
        }

        private void btnPesel_Click(object sender, EventArgs e)
        {
            bool isPeselValid = false;
            try
            {
                if (txtPesel.Text.Length == 11)
                {
                    int sum = 0;
                    for (int i = 0; i < Multipliers.Length; i++)
                    {
                        sum += Multipliers[i] * int.Parse(txtPesel.Text[i].ToString());
                    }
                    int ramaining = sum % 10;
                    string controlSum = ramaining == 0 ? ramaining.ToString() : (10 - ramaining).ToString();

                    isPeselValid = controlSum.Equals(txtPesel.Text[10].ToString());
                }
            }
            catch (Exception)
            {
                isPeselValid = false;
            }

            if (isPeselValid)
            {
                char sexCharacter = txtPesel.Text[9];
                int parsedSexCharacter = int.Parse(sexCharacter.ToString());
                if (parsedSexCharacter%2 == 0)
                {
                    lblSex.Text = "Kobieta";
                }
                else
                {
                    lblSex.Text = "Mężczyzna";
                }
                var ctx = new PeselDbDataContext();
                PeselRegistry registry = new PeselRegistry()
                {
                    Pesel = txtPesel.Text
                };
                ctx.PeselRegistries.InsertOnSubmit(registry);
                ctx.SubmitChanges();
            }
            else
            {
                lblSex.Text = "Niepoprawny Pesel";
            }
        }
    }
}
