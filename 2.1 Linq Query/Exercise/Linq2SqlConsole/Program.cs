﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linq2SqlConsole.Model;

namespace Linq2SqlConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Customer> customers = SampleDataGenerator.GenerateData();

            // Posortuj klientów po nazwisku (OrderBy)

            // Liczba klientów (Count)

            // Czy Jan jest na liście klientów (Any)

            // Czy jest jakiś klient, który posiada więcej niż jeden rachunek (Where)

            // Ile najwięcej rachunków posiada klient (Max)

            System.Console.WriteLine("Press <Enter> to stop the service.");
            System.Console.ReadLine();
        }
    }
}
