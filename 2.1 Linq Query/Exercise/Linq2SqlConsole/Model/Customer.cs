using System.Collections.Generic;

namespace Linq2SqlConsole.Model
{
    public class Customer
    {
        public Customer()
        {
            Account = new List<Account>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string PersonalId { get; set; }

        public string Phone { get; set; }

        public List<Account> Account { get; set; }
    }
}
