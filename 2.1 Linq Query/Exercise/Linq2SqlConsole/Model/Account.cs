using System.Collections.Generic;

namespace Linq2SqlConsole.Model
{
    public class Account
    {
        public Account()
        {
            OutgoingTransfers = new List<Transfer>();
            IncomingTransfers = new List<Transfer>();
        }

        public int Id { get; set; }

        public int CustomerId { get; set; }

        public decimal Balance { get; set; }

        public string AccountNumber { get; set; }

        public string Currency { get; set; }

        public virtual Customer Customer { get; set; }

        public List<Transfer> OutgoingTransfers { get; set; }

        public List<Transfer> IncomingTransfers { get; set; }
    }
}
