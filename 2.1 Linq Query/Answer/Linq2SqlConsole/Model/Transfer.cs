namespace Linq2SqlConsole.Model
{
    public class Transfer
    {
        public int Id { get; set; }

        public int AccountFromId { get; set; }

        public int AccountToId { get; set; }

        public decimal Amount { get; set; }

        public string Currency { get; set; }

        public Account AccountFrom { get; set; }

        public Account AccountTo { get; set; }
    }
}
