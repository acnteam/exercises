﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linq2SqlConsole.Model;

namespace Linq2SqlConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Customer> customers = SampleDataGenerator.GenerateData();

            // Posortuj klientów po nazwisku
            List<Customer> customersSortedBySurname = customers.OrderBy(c => c.Surname).ToList();

            // Liczba klientów
            int customerCount = customers.Count();

            // Czy Jan jest na liście klientów
            bool isJohnInCustomers = customers.Any(c => c.Name.StartsWith("Jan"));

            // Czy jest jakiś klient, który posiada więcej niż jeden rachunek
            List<Customer> customersWithMoreThanOneAccount = customers.Where(c => c.Account.Count > 1).ToList();

            // Ile najwięcej rachunków posiada klient
            int maxAccounts = customers.Max(c => c.Account.Count);

            System.Console.WriteLine("Press <Enter> to stop the service.");
            System.Console.ReadLine();
        }
    }
}
