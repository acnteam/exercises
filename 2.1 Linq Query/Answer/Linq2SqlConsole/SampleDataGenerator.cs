﻿using System.Collections.Generic;
using Linq2SqlConsole.Model;

namespace Linq2SqlConsole
{
    public class SampleDataGenerator
    {
        public static List<Customer> GenerateData()
        {
            var customers = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Name = "Jan",
                    Surname = "Kowalski",
                    PersonalId = "8301931245",
                    Phone = "432533234",
                    Account = new List<Account>
                    {
                        new Account {CustomerId = 1, AccountNumber = "001001", Balance = 100, Currency = "PLN"},
                        new Account {CustomerId = 1, AccountNumber = "001002", Balance = 0, Currency = "PLN"},
                        new Account {CustomerId = 1, AccountNumber = "001003", Balance = 0, Currency = "EUR"}
                    }
                },
                new Customer
                {
                    Id = 2,
                    Name = "Adam",
                    Surname = "Nowak",
                    PersonalId = "8301931245",
                    Phone = "432533234",
                    Account = new List<Account>
                    {
                        new Account {CustomerId = 2, AccountNumber = "002003", Balance = 50, Currency = "USD"}
                    }
                },
            };


            return customers;
        }
    }
}